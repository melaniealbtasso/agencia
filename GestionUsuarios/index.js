'use strict'

const port = process.env.PORT || 3000
const config = require('../config');
const URL_DB = config.db;
const URL_USUARIOS = `https://localhost:3000/api/usuarios`;
const URL_MOD_DB = `https://localhost:3004/api/database`;
const salt = 10;

const express = require('express');
const logger = require('morgan');
const mongojs=require('mongojs');
const fetch = require('node-fetch');
const https = require('https');
const fs = require('fs');
const bcrypt = require('bcrypt');

const app = express();

const HTTP_OPTIONS = {
    key: fs.readFileSync('../cert/key.pem'),
    cert: fs.readFileSync('../cert/cert.pem')
}

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var db = mongojs(URL_DB);
var id = mongojs.ObjectID;

app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

function auth(req, res, next) {
    if (!req.headers.authorization){
        res.status(401).json({
            result: "KO",
            mensaje: "No se ha enviado el token en la cabereca autorization"
        });
        return next(new Error("Falta el token"));
    }

    if( req.headers.authorization.split(" ")[1] === "melanie"){
        return next();
    }

    res.status(401).json({
        result:"KO",
        mensaje: "Acceso no autorizado a este servicio"
    });

    return next(new Error("Acceso no autorizado"));
};

https.createServer(HTTP_OPTIONS, app)
.listen(port, (request, response) => {
    console.log(`API GESTIÓN DE USUARIOS ejecutándose en ${URL_USUARIOS}`);
});

app.get('/api/usuarios', (req, res, next) => {

    const queURL = `${URL_MOD_DB}/usuarios`;
    console.log(queURL);

    fetch(queURL, {
        method: 'GET',
        headers: {
                    'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        res.json(json); //devuelve directamente el json
    });

});

app.get('/api/usuarios/:nombreusuario', (req, res, next) => {

    const queUsuario = req.params.nombreusuario;

    const queURL = `${URL_MOD_DB}/usuarios/${queUsuario}`;
    console.log(queURL);

    fetch(queURL, {
        method: 'GET',
        headers: {
                    'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        res.json(json); //devuelve directamente el json
    });

});

app.post('/api/usuarios', (req, res, next) => {

    const nuevoUsuario=req.body;
    const queURL = `${URL_MOD_DB}/usuarios`;

    bcrypt.hash(nuevoUsuario.password, salt, (err, hash) => {
        if(err) return next(err);

        nuevoUsuario.password = hash;


        fetch(queURL, {
                        method: 'POST',
                        body: JSON.stringify(nuevoUsuario),
                        headers: {
                                    'Content-Type': 'application/json'
                                 }
                    })
        .then(resp => resp.json())
        .then(json => {
            res.json(json); //devuelve directamente el json
        });

    });

});

app.put('/api/usuarios/:nombreusuario', (req, res, next) => {
    
    const queUsuario = req.params.nombreusuario;
    const nuevoUsuario=req.body;
    const queURL = `${URL_MOD_DB}/usuarios/${queUsuario}`;

    fetch(queURL, {
                    method: 'PUT',
                    body: JSON.stringify(nuevoUsuario),
                    headers: {
                                'Content-Type': 'application/json'
                            }
                })
    .then(resp => resp.json())
    .then(json => {
        res.json(json); //devuelve directamente el json
    });

});

app.delete('/api/usuarios/:nombreusuario', (req, res, next) => {
   
    const queUsuario = req.params.nombreusuario;
    const nuevoUsuario=req.body;

    const queURL = `${URL_MOD_DB}/usuarios/${queUsuario}`;

    fetch(queURL, {
                        method: 'DELETE',
                        body: JSON.stringify(nuevoUsuario),
                        headers: {
                                    'Content-Type': 'application/json'
                                 }
                    })
    .then(resp => resp.json())
    .then(json => {
        res.json(json); //devuelve directamente el json
    });

});

app.post('/api/login', (req, res, next) => {

    const loginUsuario=req.body;

    const queURL = `${URL_MOD_DB}/login`;

    fetch(queURL, {
                        method: 'POST',
                        body: JSON.stringify(loginUsuario),
                        headers: {
                                    'Content-Type': 'application/json'
                                 }
                    })
        .then(resp => resp.json())
        .then(json => {
            res.status(200).json(json); //devuelve directamente el json
        })
});