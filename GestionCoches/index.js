'use strict'

const port = process.env.PORT || 3001
const config = require('../config');
const URL_PROVCOCHES = `https://localhost:3011/api/provcoches`;
const URL_USUARIOS = `https://localhost:3000/api/usuarios`;
const URL_COCHES = `https://localhost:${port}/api/coches`;
const URL_MOD_DB = `https://localhost:3004/api/database`;
const URL_DB = config.db;

const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const querystring = require('query-string');
const fetch = require('node-fetch');
const { json } = require('express');
const https = require('https');
const fs = require('fs');

const HTTP_OPTIONS = {
    key: fs.readFileSync('../cert/key.pem'),
    cert: fs.readFileSync('../cert/cert.pem')
}
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const app = express();

// var db = mongojs(URL_DB);
// var id = mongojs.ObjectID;

const helmet = require('helmet');
app.use(helmet());
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

function auth(req, res, next) {
    if (!req.headers.authorization){
        res.status(401).json({
            result: "KO",
            mensaje: "No se ha enviado el token en la cabereca autorization"
        });
        return next(new Error("Falta el token"));
    }

    if( req.headers.authorization.split(" ")[1] === "melanie"){
        return next();
    }

    res.status(401).json({
        result:"KO",
        mensaje: "Acceso no autorizado a este servicio"
    });

    return next(new Error("Acceso no autorizado"));
};

https.createServer(HTTP_OPTIONS, app)
.listen(port, (request, response) => {
    console.log(`API GESTIÓN DE COCHES ejecutándose en ${URL_COCHES}`);
});

app.get('/api/coches/', (req, res, next) => {

    const queries = req.url.split("?")[1];
    console.log(`${queries}`);
    const urlFetch = URL_PROVCOCHES + "?" + queries;

    console.log(`${urlFetch}`);

    fetch(`${urlFetch}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        res.json(json);
        console.log(` Las ofertas se han encontrado!!`);
    });
});

app.get('/api/coches/:nombreusuario', (req, res, next) => {

    const nombreusuario = req.params.nombreusuario;

    fetch(`${URL_MOD_DB}/coches/${nombreusuario}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        res.status(200).json(json);
        console.log(` Las ofertas se han encontrado!!`);
    });

});

app.post('/api/coches', (req, res, next) => {

    const nuevaReserva=req.body;

    const queURL = `${URL_MOD_DB}/coches`;

    fetch(`${URL_PROVCOCHES}`, {
        method: 'POST',
        body: JSON.stringify(nuevaReserva),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        delete json['resultado'];
        json.idPaquete = nuevaReserva.idPaquete;
        json.nombrereserva = nuevaReserva.nombrereserva;
        json.nombreusuario = nuevaReserva.nombreusuario;
        json.idReserva = nuevaReserva.idReserva;
        //res.json(json);
        console.log(` Reserva efectuada del coche con id ${json.idCoche} para el cliente ${json.nombrereserva}`);
        
        fetch(queURL, {
            method: 'POST',
            body: JSON.stringify(nuevaReserva),
            headers: {
                        'Content-Type': 'application/json' 
                     }
        })
        .then(resp => resp.json())
        .then(json => {
            res.json(json); //devuelve directamente el json
        });

    });

});

app.delete('/api/coches/:idCoche', (req, res, next) => {

    const idCoche = req.params.idCoche;
    const queURL = `${URL_MOD_DB}/coches/${idCoche}`;

    fetch(`${URL_PROVCOCHES}/${idCoche}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {

        fetch(queURL, {
            method: 'DELETE',
            headers: {
                        'Content-Type': 'application/json' 
                     }
        })
        .then(resp => resp.json())
        .then(json => {
            res.json(json); //devuelve directamente el json
        });

    });

});







