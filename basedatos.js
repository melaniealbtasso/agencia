'use strict'

const config = require('../config');
const servicioToken = require('../Servicios/servicioToken');

const port = process.env.PORT || 3004
const URL_PROVCOCHES = `https://localhost:3011/api/provcoches`;
const URL_PROVHOTELES = `https://localhost:3013/api/provhoteles`;
const URL_PROVVUELOS = `https://localhost:3012/api/provvuelos`;
const URL_USUARIOS = `https://localhost:3000/api/usuarios`;
const URL_COCHES = `https://localhost:3001/api/coches`;
const URL_MOD_DB = `https://localhost:3004/api/database`;
const URL_DB = config.db;
const salt = 10;

const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const fetch = require('node-fetch');
const { json } = require('express');
const https = require('https');
const fs = require('fs');
const helmet = require('helmet');
const bcrypt = require('bcrypt');

const HTTP_OPTIONS = {
    key: fs.readFileSync('../cert/key.pem'),
    cert: fs.readFileSync('../cert/cert.pem')
}
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const app = express();

var db = mongojs(URL_DB);
var id = mongojs.ObjectID;

app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

function auth(req, res, next) {
    if (!req.headers.authorization){
        res.status(401).json({
            result: "KO",
            mensaje: "No se ha enviado el token en la cabereca autorization"
        });
        return next(new Error("Falta el token"));
    }

    if( req.headers.authorization.split(" ")[1] === "melanie"){
        return next();
    }

    res.status(401).json({
        result:"KO",
        mensaje: "Acceso no autorizado a este servicio"
    });

    return next(new Error("Acceso no autorizado"));
};

https.createServer(HTTP_OPTIONS, app)
.listen(port, (request, response) => {
    console.log(`API GESTIÓN DE BASE DE DATOS ejecutándose en ${URL_MOD_DB}`);
});

/**
 * BASE DE DATOS DE USUARIO
 */

// app.get('/api/database/usuarios', (req, res, next) => {
    
//     db.usuarios.find((err, elementos) =>{
//         if(err) return next(err);
        
//         console.log(elementos);
//         res.json({
//             result: 'OK',
//             coleccion: 'usuarios',
//             elemetos: elementos
//         });
//     });
// });

app.get('/api/database/usuarios/:nombreusuario', (req, res, next) => {

    const nombreusuario = req.params.nombreusuario

    db.usuarios.findOne({nombreusuario: nombreusuario}, (err, usuario) => {
        if(err) return next(err);
        
        console.log(usuario);
        res.json({
            result: 'OK',
            coleccion: 'usuarios',
            usuario: usuario
        });
    });
});

app.post('/api/database/usuarios', (req, res, next) => {
    const nuevoUsuario=req.body;
    //const queColeccion=req.params.usuarios;

    bcrypt.hash(nuevoUsuario.password, salt, (err, hash) => {
        if(err) return next(err);

        nuevoUsuario.password = hash;

        db.usuarios.save(nuevoUsuario, (err, usuarioGuardado) => {
            if(err) return next(err);
    
            console.log(usuarioGuardado);
            res.status(201).json({
                result: 'OK',
                usuario: usuarioGuardado
            });
        });
        
    });

});

app.put('/api/database/usuarios/:nombreusuario', (req, res, next) => {
    
    const nuevosDatos=req.body;
    const nombreusuario = req.params.nombreusuario;

    db.usuarios.update(
        {nombreusuario: nombreusuario},
        {$set: nuevosDatos},
        {safe: true, multi: false},
        (err, resultado) => {
            if(err) return next(err);

            console.log(resultado);
            res.json({
                result: 'OK',
                coleccion: 'usuarios',
                resultado: resultado
            });
        
    });
});

app.delete('/api/database/usuarios/:nombreusuario', (req, res, next) => {
    
    const nombreusuario = req.params.nombreusuario;
    //const queColeccion=req.params.colecciones;

    db.usuarios.remove(
        {nombreusuario: nombreusuario},
        (err, resultado) => {
            if(err) return next(err);

            console.log(resultado);
            res.json({
                result: 'OK',
                coleccion: 'usuarios',
                usuarioeliminado: nombreusuario,
                resultado: resultado
            });
        
    });
});

app.post('/api/database/login', (req, res, next) => {

    const nombreusuario = req.body.nombreusuario;
    const password = req.body.password;

    db.usuarios.findOne({nombreusuario: nombreusuario}, (err, usuario) => {
        if(err || !usuario) {

            console.log(`No se ha encontrado en la BD el usuario ${nombreusuario}`);

            res.status(404).json({
                result: 'KO',
                error: "Usuario no registrado"
            });

        } else {
            bcrypt.compare(password, usuario.password, (erro, OKpassword) => {

                if(err) return next(err);

                if (!OKpassword) {

                    console.log(`Contraseña incorrecta para el usuario ${nombreusuario}`);

                    res.status(404).json({
                        result: 'KO',
                        error: "Contraeña incorrecta"
                    });

                } else {
                    
                    console.log(`El usuario ${nombreusuario} ha iniciado sesión`);

                    const token = servicioToken.crearToken(usuario);
                    servicioToken.comprobarToken(token)
                    .then(resultado => {
                        res.status(200).json({
                            result: 'OK',
                            usuario: resultado,
                            token: token
                        });
                    })
                }
            });
        }  

    });

});

/**
 * BASE DE DATOS DE COCHES
 */

app.get('/api/database/coches/:nombreusuario', (req, res, next) => {

    const nombreusuario = req.params.nombreusuario;

    db.coches.find({nombreusuario: nombreusuario}, (err, reservas) => {
        if(err) return next(err);
        
        console.log(reservas);
        res.json({
            result: 'OK',
            coleccion: 'coches',
            reservas: reservas
        });
    });
});

app.post('/api/database/coches', (req, res, next) => {
    const nuevaReserva = req.body;

    fetch(`${URL_PROVCOCHES}`, {
        method: 'POST',
        body: JSON.stringify(nuevaReserva),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        delete json['resultado'];
        json.idPaquete = nuevaReserva.idPaquete;
        json.nombrereserva = nuevaReserva.nombrereserva;
        json.nombreusuario = nuevaReserva.nombreusuario;
        json.idReserva = nuevaReserva.idReserva;
        res.json(json);
        console.log(` Reserva efectuada del coche con id ${json.id} para el cliente ${json.nombrereserva}`);
        db.coches.save(json);
    });

});

app.delete('/api/database/coches/:idCoche', (req, res, next) => {

    const idCoche = req.params.idCoche;

    fetch(`${URL_PROVCOCHES}/${idCoche}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        db.coches.remove(
            {idCoche: idCoche, proveedor: json.proveedor},
            (err, resultado) => {
                if (err) return next(err);

                res.status(200).json({
                            result:'OK',
                            idCoche: idCoche,
                            resultado: resultado
                });
            
        });
    });
});

/**
 * GESTIÓN de BASE DE DATOS VUELOS
 */

app.get('/api/database/vuelos/:nombreusuario', (req, res, next) => {

    const nombreusuario = req.params.nombreusuario

    db.vuelos.find({nombreusuario: nombreusuario}, (err, reservas) => {
        if(err) return next(err);
        
        console.log(reservas);
        res.json({
            result: 'OK',
            coleccion: 'vuelos',
            reservas: reservas
        });
    });
});

app.post('/api/database/vuelos', (req, res, next) => {
    const nuevaReserva = req.body;

    fetch(`${URL_PROVVUELOS}`, {
        method: 'POST',
        body: JSON.stringify(nuevaReserva),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        delete json['resultado'];
        json.idPaquete = nuevaReserva.idPaquete;
        json.nombrereserva = nuevaReserva.nombrereserva;
        json.nombreusuario = nuevaReserva.nombreusuario;
        res.json(json);
        console.log(` Reserva efectuada del vuelo con id ${json.id} para el cliente ${json.nombrereserva}`);
        db.vuelos.save(json);
    });

});

app.delete('/api/database/vuelos/:proveedor/:idvuelo', (req, res, next) => {

    const idvuelo = req.params.idvuelo;

    fetch(`${URL_PROVVUELOS}/${idvuelo}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        db.vuelos.remove(
            {idvuelo: idvuelo, proveedor: json.proveedor},
            (err, resultado) => {
                if (err) return next(err);

                res.status(200).json({
                            result:'OK',
                            idvuelo: idvuelo,
                            resultado: resultado
                });
            
        });
    });
});

/**
 * GESTIÓN BASE DE DATOS HOTELES
 */

app.get('/api/database/hoteles/:nombreusuario', (req, res, next) => {

    const nombreusuario = req.params.nombreusuario

    db.hoteles.find({nombreusuario: nombreusuario}, (err, reservas) => {
        if(err) return next(err);
        
        console.log(reservas);
        res.json({
            result: 'OK',
            coleccion: 'hoteles',
            reservas: reservas
        });
    });
});

app.post('/api/database/hoteles', (req, res, next) => {

    const nuevaReserva = req.body;

    fetch(`${URL_PROVHOTELES}`, {
        method: 'POST',
        body: JSON.stringify(nuevaReserva),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        delete json['resultado'];
        json.idPaquete = nuevaReserva.idPaquete;
        json.nombrereserva = nuevaReserva.nombrereserva;
        json.nombreusuario = nuevaReserva.nombreusuario;
        res.json(json);
        console.log(` Reserva efectuada del hotel con id ${json.id} para el cliente ${json.nombrereserva}`);
        db.hoteles.save(json);
    });

});

app.delete('/api/database/hoteles/:proveedor/:idHotel', (req, res, next) => {

    const idHotel = req.params.idHotel;

    fetch(`${URL_PROVHOTELES}/${idHotel}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        db.hoteles.remove(
            {idHotel: idHotel, proveedor: json.proveedor},
            (err, resultado) => {
                if (err) return next(err);

                res.status(200).json({
                            result:'OK',
                            idHotel: idHotel,
                            resultado: resultado
                });
            
        });
    });
});

