'use strict'

const port = process.env.PORT || 3013
const URL_PROVHOTELES = `https://localhost:3013/api/provhoteles`

const express = require('express');
const logger = require('morgan');
//const mongojs = require('mongojs');
const querystring = require('query-string');
const https = require('https');
const fs = require('fs');
const moment = require('moment');

const HTTP_OPTIONS = {
    key: fs.readFileSync('../cert/key.pem'),
    cert: fs.readFileSync('../cert/cert.pem')
}
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const app = express();

//var db = mongojs("127.0.0.1/agencia");
//var id = mongojs.ObjectID;

app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

function oferID(min, max) {
    return Math.floor(Math.random() * (max + min) + min);
}

function precio(minPrice, maxPrice) {
    const price = (Math.random() * (maxPrice - minPrice) + Number(minPrice));
    return price - (price%0.01);
}

function filterObj(source, whiteList) {
    const res = {};
    
    Object.keys(source).forEach((key) => {
      
    if (whiteList.indexOf(key) !== -1) {
            res[key] = source[key];
        }
    });
    return res;
};

function auth(req, res, next) {
    if (!req.headers.authorization){
        res.status(401).json({
            result: "KO",
            mensaje: "No se ha enviado el token en la cabereca autorization"
        });
        return next(new Error("Falta el token"));
    }

    if( req.headers.authorization.split(" ")[1] === "melanie"){
        return next();
    }

    res.status(401).json({
        result:"KO",
        mensaje: "Acceso no autorizado a este servicio"
    });

    return next(new Error("Acceso no autorizado"));
};

https.createServer(HTTP_OPTIONS, app)
.listen(port, (request, response) => {
    console.log(`API PROVEEDORES DE HOTELES ejecutándose en ${URL_PROVHOTELES}`);
});

app.get('/api/provhoteles/', (req, res, next) => {
     
     // This array will serve as a whitelist to select keys you want to keep in rawJson
     const filterArray = [
       "ciudad",
       "fechaInicio",
       "fechaFinal",
       "personas",
       "precMax",
       "precMin"
     ];

    const queries = req.query;
    const filtro = filterObj(queries, filterArray);
    
    filtro.idHotel = oferID(1000000, 9999999);

    if(queries.precMin&&queries.precMax){
        filtro.precio = precio(queries.precMin, queries.precMax);
    } else if (queries.fechaInicio&&queries.fechaFinal){
        const diasReserva = moment(queries.fechaFinal).diff(queries.fechaInicio)/86400000;
        if(queries.personas){
            filtro.precio = 49.99*diasReserva*queries.personas;
        } else {
            filtro.precio = 49.99*diasReserva;
        }    
    } else {
        filtro.precio = 349.99
    }

    res.status(200).json({
        
        resultado: 'OK',
        queries: filtro

    });
    
});

app.post('/api/provhoteles/', (req, res, next) => {

   res.status(200).json({
       
       resultado: 'OK',
       idHotel: req.body.idHotel,
       dni: req.body.dni,
       nombrereserva: req.body.nombrereserva,
       proveedor: 6654

   });
   
});

app.delete('/api/provhoteles/:idHotel', (req, res, next) => {

    res.status(200).json({
        
        resultado: 'OK',
        idHotel: req.body.idHotel,
        proveedor: 6654
 
    });
    
 });
