'use strict'

const port = process.env.PORT || 3006;
const URL_PAQUETES = `https://localhost:${port}/api/paquetes`;

const config = require('../config');

const servicios = {
    usuarios: "https://localhost:3000/api/usuarios",
    coches: "https://localhost:3001/api/coches",
    vuelos: "https://localhost:3002/api/vuelos",
    hoteles: "https://localhost:3003/api/hoteles",
    pagos: "https://localhost:3005/api/pagos"
}

const URL_MOD_DB = `https://localhost:3004/api/database`;
const URL_PAGOS = `https://localhost:3005/api/pagos`;
const URL_LOGIN = `https://localhost:3000/api/login`;

const servicioToken = require('../Servicios/servicioToken');
const URL_DB = config.db;

const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const https = require('https');
const fs = require('fs');
const moment = require('moment');
const fetch = require('node-fetch');
const { url } = require('inspector');

const app = express();
var db = mongojs(URL_DB);

const HTTP_OPTIONS = {
    key: fs.readFileSync('../cert/key.pem'),
    cert: fs.readFileSync('../cert/cert.pem')
}

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const helmet = require('helmet');
app.use(helmet());
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

function oferID(min, max) {
    return Math.floor(Math.random() * (max + min) + min);
}

function precio(minPrice, maxPrice) {
    const price = (Math.random() * (maxPrice - minPrice) + Number(minPrice));
    return price - (price%0.01);
}

https.createServer(HTTP_OPTIONS, app)
.listen(port, (request, response) => {
    console.log(`API DE  PAQUETES ejecutándose en ${URL_PAQUETES}`);
});

app.get('/api/paquetes/:nombreusuario', (req, res, next) => {

    const queUsuario = req.params.nombreusuario;

    const queURL = `${URL_MOD_DB}/paquetes/${queUsuario}`;
    console.log(queURL);

    fetch(queURL, {
        method: 'GET',
        headers: {
                    'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        res.json(json); //devuelve directamente el json
    });

});

// Crear un nuevo elemento en una coleeción
// app.post('/api/paquetes', (req, res, next) => {

//     const nuevoPaquete = req.body;
//     const coches = nuevoPaquete.coches;
//     const hoteles = nuevoPaquete.hoteles;
//     const vuelos = nuevoPaquete.vuelos;
//     nuevoPaquete.idPaquete = oferID(1000000, 9999999).toString();

//     const queURL = `${URL_MOD_DB}/paquetes`;
//     console.log(queURL);

//     fetch(queURL, {
//         method: 'POST',
//         body: JSON.stringify(nuevoPaquete),
//         headers: {
//                     'Content-Type': 'application/json'
//         }
//     })

    // delete nuevoPaquete['coches'];
    // delete nuevoPaquete['hoteles'];
    // delete nuevoPaquete['vuelos'];

    // for(let i=0; i < coches.length; i++) {
    //     nuevoPaquete.idCoche = coches[i].idCoche;
    //     const queURL = servicios["coches"];
    //     //console.log (`${nuevoPaquete.idCoche} ${nuevoPaquete.nombreusuario}  --- ${nuevoPaquete.coches}`);
    //     fetch(queURL, {
    //         method: 'POST',
    //         body: JSON.stringify(nuevoPaquete),
    //         headers: {
    //             'Content-Type': 'application/json'
    //         }
    //     })
        
    // }

    // delete nuevoPaquete['idCoche'];

    // for(let i=0; i < hoteles.length; i++) {
    //     nuevoPaquete.idHotel = hoteles[i].idHotel;
//         const queURL = servicios["hoteles"];
//         //console.log (`${nuevoPaquete.idCoche} ${nuevoPaquete.nombreusuario}  --- ${nuevoPaquete.coches}`);
//         fetch(queURL, {
//             method: 'POST',
//             body: JSON.stringify(nuevoPaquete),
//             headers: {
//                 'Content-Type': 'application/json'
//             }
//         })
//     }

//     delete nuevoPaquete['idHotel'];

//     for(let i=0; i < vuelos.length; i++) {
//         nuevoPaquete.idVuelo = vuelos[i].idVuelo;
//         const queURL = servicios["vuelos"];
//         //console.log (`${nuevoPaquete.idCoche} ${nuevoPaquete.nombreusuario}  --- ${nuevoPaquete.coches}`);
//         fetch(queURL, {
//             method: 'POST',
//             body: JSON.stringify(nuevoPaquete),
//             headers: {
//                 'Content-Type': 'application/json'
//             }
//         })
//     }

//     res.status(200).json({
//         result:'OK',
//         idPaquete: nuevoPaquete.idPaquete
//     })

// });


// Eliminar un elemento de una colleción
app.delete('/api/paquetes/:idPaquete', (req, res, next) => {

    const idPaquete = req.params.idPaquete;

    db.paquetes.findOne({idPaquete: idPaquete}, (err, nuevoPaquete) => {
        if(err || !nuevoPaquete) return next(err);
        
        console.log(nuevoPaquete);
        
        const coches = nuevoPaquete.coches;
        const hoteles = nuevoPaquete.hoteles;
        const vuelos = nuevoPaquete.vuelos;

        delete nuevoPaquete['coches'];
        delete nuevoPaquete['hoteles'];
        delete nuevoPaquete['vuelos'];

        for(let i=0; i < coches.length; i++) {
            //nuevoPaquete.idCoche = coches[i].idCoche;
            const queURL = servicios["coches"];
            //console.log (`${nuevoPaquete.idCoche} ${nuevoPaquete.nombreusuario}  --- ${nuevoPaquete.coches}`);
            fetch(`${queURL}/${coches[i].idCoche}`, {
                                method: 'DELETE',
                                headers: {
                                    'Content-Type': 'application/json'
                                }
            })
        
        }

        for(let i=0; i < vuelos.length; i++) {
            //nuevoPaquete.idCoche = coches[i].idCoche;
            const queURL = servicios["vuelos"];
            //console.log (`${nuevoPaquete.idCoche} ${nuevoPaquete.nombreusuario}  --- ${nuevoPaquete.coches}`);
            fetch(`${queURL}/${vuelos[i].idVuelo}`, {
                                method: 'DELETE',
                                headers: {
                                    'Content-Type': 'application/json'
                                }
            });
        
        }

        for(let i=0; i < hoteles.length; i++) {
            //nuevoPaquete.idCoche = coches[i].idCoche;
            const queURL = servicios["hoteles"];
            //console.log (`${nuevoPaquete.idCoche} ${nuevoPaquete.nombreusuario}  --- ${nuevoPaquete.coches}`);
            fetch(`${queURL}/${hoteles[i].idHotel}`, {
                                method: 'DELETE',
                                headers: {
                                    'Content-Type': 'application/json'
                                }
            });
        
        }

    });

    fetch(`${URL_MOD_DB}/paquetes/${idPaquete}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        res.json(json); //devuelve directamente el json
    });
    
});

app.post('/api/paquetes', (req, res, next) => {

    const nuevoPaquete = req.body;
    const tarjeta = nuevoPaquete.tarjeta;
    const coches = nuevoPaquete.coches;
    const hoteles = nuevoPaquete.hoteles;
    const vuelos = nuevoPaquete.vuelos;
    nuevoPaquete.idPaquete = oferID(1000000, 9999999).toString();

    const queURL = `${URL_MOD_DB}/paquetes`;
    console.log(queURL);

    fetch(queURL, {
        method: 'POST',
        body: JSON.stringify(nuevoPaquete),
        headers: {
                    'Content-Type': 'application/json'
        }
    })

    delete nuevoPaquete['coches'];
    delete nuevoPaquete['hoteles'];
    delete nuevoPaquete['vuelos'];
    delete nuevoPaquete['tarjeta'];

    //delete nuevoPaquete['idCoche'];

    for(let i = 0; i < coches.length; i++) {
        nuevoPaquete.idCoche = coches[i].idCoche;
        const queURL = servicios["coches"];
        //console.log (`${nuevoPaquete.idCoche} ${nuevoPaquete.nombreusuario}  --- ${nuevoPaquete.coches}`);
        fetch(queURL, {
            method: 'POST',
            body: JSON.stringify(nuevoPaquete),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(resp => resp.json())
        .then(json => { 

            if(json.result === 'KO') {

                fetch(`${URL_PAQUETES}/${nuevoPaquete.idPaquete}`, {
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .then(res => res.json())
                .then(json => {
                    res.json(json);
                });
            }
            
        });
    }

    delete nuevoPaquete['idCoche'];

    for(let i = 0; i < hoteles.length; i++) {
        nuevoPaquete.idHotel = hoteles[i].idHotel;
        const queURL = servicios["hoteles"];
        //console.log (`${nuevoPaquete.idCoche} ${nuevoPaquete.nombreusuario}  --- ${nuevoPaquete.coches}`);
        fetch(queURL, {
            method: 'POST',
            body: JSON.stringify(nuevoPaquete),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(resp => resp.json())
        .then(json => { 
    
            if(json.result === 'KO') {
    
                fetch(`${URL_PAQUETES}/${nuevoPaquete.idPaquete}`, {
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .then(res => res.json())
                .then(json => {
                        res.json(json);
                });
            }
                
        });
    }

    delete nuevoPaquete['idHotel'];

    for(let i = 0; i < vuelos.length; i++) {
        nuevoPaquete.idVuelo = vuelos[i].idVuelo;
        const queURL = servicios["vuelos"];
        //console.log (`${nuevoPaquete.idCoche} ${nuevoPaquete.nombreusuario}  --- ${nuevoPaquete.coches}`);
        fetch(queURL, {
            method: 'POST',
            body: JSON.stringify(nuevoPaquete),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(resp => resp.json())
        .then(json => { 
    
            if(json.result === 'KO') {
    
                fetch(`${URL_PAQUETES}/${nuevoPaquete.idPaquete}`, {
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .then(res => res.json())
                .then(json => {
                        res.json(json);
                });
            }
                
        });
    }

    delete nuevoPaquete['idVuelo'];
    delete nuevoPaquete['nombrereserva'];
    delete nuevoPaquete['dni'];

    nuevoPaquete.cantidad = precio(100, 5000);

    fetch(`${URL_PAGOS}`, {
        method: 'POST',
        body: JSON.stringify(nuevoPaquete),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(resp => resp.json())
        .then(json => { 
    
            if(json.result === 'KO') {
    
                fetch(`${URL_PAQUETES}/${nuevoPaquete.idPaquete}`, {
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .then(res => res.json())
                .then(json => {
                        res.json(json);
                });
            } else {
                res.status(200).json({
                    idPaquete: nuevoPaquete.idPaquete
                });
            }
                
        });


});
