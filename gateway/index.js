'use strict'

const port = process.env.PORT || 3100
const URL_GATEWAY = `https://localhost:${port}/api`;

const servicios = {
    usuarios: "https://localhost:3000/api/usuarios",
    coches: "https://localhost:3001/api/coches",
    vuelos: "https://localhost:3002/api/vuelos",
    hoteles: "https://localhost:3003/api/hoteles",
    pagos: "https://localhost:3005/api/pagos",
    paquetes: "https://localhost:3006/api/paquetes"
}

const URL_LOGIN = `https://localhost:3000/api/login`;

const servicioToken = require('../Servicios/servicioToken');

const express = require('express');
const logger = require('morgan');
const mongojs=require('mongojs');
const https = require('https');
const fs = require('fs');
const moment = require('moment');
const fetch = require('node-fetch');
const { url } = require('inspector');

const app = express();

const HTTP_OPTIONS = {
    key: fs.readFileSync('../cert/key.pem'),
    cert: fs.readFileSync('../cert/cert.pem')
}

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const helmet = require('helmet');
app.use(helmet());
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

// function auth(req, res, next) {

//     if (!req.headers.authorization){
//         res.status(401).json({
//             result: "KO",
//             mensaje: "No se ha enviado el token en la cabereca autorization"
//         });
//         return next(new Error("Falta el token"));
//     }
//     console.log(` ${req.body.nombreusuario} `);
//     const token = req.headers.authorization.split(" ")[1];
//     servicioToken.comprobarToken(token).then(nombreusuario => {

//         if(req.body.nombreusuario != null && req.params.id != null) {
//             console.log("1");
//             if (req.body.nombreusuario == nombreusuario && req.params.id == nombreusuario) {
//             console.log("2");
//                 return next();
//             } else {
//                 res.status(401).json({
//                     result:"KO",
//                     mensaje: "TOKEN INCORRECTO"
//                 });
//             }
//         } else if (req.params.id == nombreusuario || req.body.nombreusuario == nombreusuario) {
//         console.log("3");
//             return next();
//         } else {
//            // console.log(`${nombreusuario}  ${req.body.nombreusuario} 2`);
//             res.status(401).json({
//                 result:"KO",
//                 mensaje: "TOKEN INCORRECTO"
//             });
//         }

//     })
//     .catch (err => {

//         console.log(err);

//         res.status(401).json({
//             result:"KO",
//             mensaje: "Token Incorrecto"
//         });
//     });

// }

function auth(req, res, next) {

    if (!req.headers.authorization){
        res.status(401).json({
            result: "KO",
            mensaje: "No se ha enviado el token en la cabereca autorization"
        });
        return next(new Error("Falta el token"));
    }

    const token = req.headers.authorization.split(" ")[1];
    servicioToken.comprobarToken(token).then(nombreusuario => {

        if(req.params.id != null && req.params.id == nombreusuario) {
            return next();
        } else {
            res.status(401).json({
                result:"KO",
                mensaje: "TOKEN INCORRECTO"
            });
        }
    })
    .catch (err => {

        console.log(err);

        res.status(401).json({
            result:"KO",
            mensaje: "TOKEN INCORRECTO"
        });
    });
}

function authBody(req, res, next) {

    if (!req.headers.authorization){
        res.status(401).json({
            result: "KO",
            mensaje: "No se ha enviado el token en la cabereca autorization"
        });
        return next(new Error("Falta el token"));
    }

    const token = req.headers.authorization.split(" ")[1];
    servicioToken.comprobarToken(token).then(nombreusuario => {

        if(req.body.nombreusuario != null && req.body.nombreusuario == nombreusuario) {
            return next();
        } else {
            res.status(401).json({
                result:"KO",
                mensaje: "TOKEN INCORRECTO"
                });
        }
    })
    .catch (err => {

        console.log(err);

        res.status(401).json({
            result:"KO",
            mensaje: "TOKEN INCORRECTO"
        });
    });
}

function authTodo(req, res, next) {

    if (!req.headers.authorization){
        res.status(401).json({
            result: "KO",
            mensaje: "No se ha enviado el token en la cabereca autorization"
        });
        return next(new Error("Falta el token"));
    }

    const token = req.headers.authorization.split(" ")[1];
    if(token === "uno1dos2tres3") {
            return next();
    } else {
        res.status(401).json({
        result:"KO",
        mensaje: "TOKEN INCORRECTO"
        });
    }
}

https.createServer(HTTP_OPTIONS, app)
.listen(port, (request, response) => {
    console.log(`API GATEWAY ejecutándose en ${URL_GATEWAY}`);
});

//Muestra todos los usuarios
app.get('/api/usuarios', authTodo, (req, res, next) => {

    const queURL = servicios["usuarios"];
    console.log(queURL);

    fetch(queURL, {
        method: 'GET',
        headers: {
                    'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        res.status(200).json(json); //devuelve directamente el json
    });
});

//Crear un nuevo usuario
app.post('/api/usuarios', (req, res, next) => {

    const nuevoUsuario = req.body;

    const queURL = servicios["usuarios"];

    fetch(queURL, {
                        method: 'POST',
                        body: JSON.stringify(nuevoUsuario),
                        headers: {
                            'Content-Type': 'application/json' 
                        }
                    })
        .then(resp => resp.json())
        .then(json => {
            res.status(200).json(json); //devuelve directamente el json
        })
});
 
app.post('/api/login', (req, res, next) => {

    const loginUsuario=req.body;

    const queURL = `${URL_LOGIN}`;

    fetch(queURL, {
                        method: 'POST',
                        body: JSON.stringify(loginUsuario),
                        headers: {
                                    'Content-Type': 'application/json'
                                 }
    })
    .then(resp => resp.json())
    .then(json => {
        res.status(200).json(json); //devuelve directamente el json
    });
});

//  Obtener elementos que presente en una colleción con ese identificador
app.get('/api/:collecion/:id', auth, (req, res, next) => {

    const collecion = req.params.collecion;
    const id = req.params.id;

    const queURL = servicios[collecion];

    console.log(`${queURL}/${id}`);

    fetch(`${queURL}/${id}`, {
                                method: 'GET',
                                headers: {
                                    'Content-Type': 'application/json'
                                }
    })
    .then(res => res.json())
    .then(json => {
        res.json(json);
    });

});

// Obtener las diferentes ofertas de un servicio
app.get('/api/:collecion/', (req, res, next) => {

    const collecion = req.params.collecion;
    const queries = req.url.split("?")[1];
    console.log(`${queries}`);

    const urlFetch = servicios[collecion];

    fetch(`${urlFetch}?${queries}`, {
                                        method: 'GET',
                                        headers: {
                                            'Content-Type': 'application/json'
                                        }
    })
    .then(res => res.json())
    .then(json => {
        res.status(200).json(json);
        console.log("Las ofertas se han encontrado.");
    });

});

// Crear un nuevo elemento en una coleeción
app.post('/api/:collecion', authBody, (req, res, next) => {

    const nuevoElemento = req.body;
    const collecion = req.params.collecion;

    const queURL = servicios[collecion];

    fetch(queURL, {
                        method: 'POST',
                        body: JSON.stringify(nuevoElemento),
                        headers: {
                            'Content-Type': 'application/json'
                        }
    })
    .then(res => res.json())
    .then(json => {
            res.json(json);
    });

});

// Modificar un elemento de una colleción
app.put('/api/:collecion/:id', auth, (req, res, next) => {

    const collecion = req.params.collecion;
    const id = req.params.id;
    const modificaciones = req.body;

    const queURL = servicios[collecion];

    fetch(`${queURL}/${id}`, {
                                method: 'PUT',
                                body: JSON.stringify(modificaciones),
                                headers: {
                                    'Content-Type': 'application/json'
                                }
    })
    .then(res => res.json())
    .then(json => {
        res.json(json);
    });

});

// Eliminar un elemento de una colleción
app.delete('/api/:collecion/:id', authBody, (req, res, next) => {

    const collecion = req.params.collecion;
    const id = req.params.id;
    console.log(`${id}`);

    const queURL = servicios[collecion];
 

    fetch(`${queURL}/${id}`, {
                                method: 'DELETE',
                                headers: {
                                    'Content-Type': 'application/json'
                                }
    })
    .then(res => res.json())
    .then(json => {
        res.json(json);
    });

});


