'use strict'

const port = process.env.PORT || 3003
const URL_PROVHOTELES = `https://localhost:3013/api/provhoteles`;
const URL_USUARIOS = `https://localhost:3000/api/usuarios`;
const URL_HOTELES = `https://localhost:${port}/api/hoteles`;
const URL_MOD_DB = `https://localhost:3004/api/database`;

const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const querystring = require('query-string');
const fetch = require('node-fetch');
const { json } = require('express');
const https = require('https');
const fs = require('fs');

const HTTP_OPTIONS = {
    key: fs.readFileSync('../cert/key.pem'),
    cert: fs.readFileSync('../cert/cert.pem')
}
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const app = express();

// var db = mongojs("127.0.0.1/agencia");
// var id = mongojs.ObjectID;

const helmet = require('helmet');
app.use(helmet());
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

function auth(req, res, next) {
    if (!req.headers.authorization){
        res.status(401).json({
            result: "KO",
            mensaje: "No se ha enviado el token en la cabereca autorization"
        });
        return next(new Error("Falta el token"));
    }

    if( req.headers.authorization.split(" ")[1] === "melanie"){
        return next();
    }

    res.status(401).json({
        result:"KO",
        mensaje: "Acceso no autorizado a este servicio"
    });

    return next(new Error("Acceso no autorizado"));
};

https.createServer(HTTP_OPTIONS, app)
.listen(port, (request, response) => {
    console.log(`API GESTIÓN DE HOTELES ejecutándose en ${URL_HOTELES}`);
});

app.get('/api/hoteles/', (req, res, next) => {

    const queries = req.url.split("?")[1];
    console.log(`${queries}`);
    const urlFetch = URL_PROVHOTELES + "?" + queries;

    console.log(`${urlFetch}`);

    fetch(`${urlFetch}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        res.json(json);
        console.log(` Las ofertas se han encontrado!!`);
    });
});

app.get('/api/hoteles/:nombreusuario', (req, res, next) => {

    const nombreusuario = req.params.nombreusuario;

    fetch(`${URL_MOD_DB}/hoteles/${nombreusuario}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        res.json(json);
        console.log(` Las ofertas se han encontrado!!`);
    });

});

app.post('/api/hoteles', (req, res, next) => {

    const nuevaReserva=req.body;

    const queURL = `${URL_MOD_DB}/hoteles`;

    fetch(`${URL_PROVHOTELES}`, {
        method: 'POST',
        body: JSON.stringify(nuevaReserva),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        delete json['resultado'];
        json.idPaquete = nuevaReserva.idPaquete;
        json.nombrereserva = nuevaReserva.nombrereserva;
        json.nombreusuario = nuevaReserva.nombreusuario;
        //res.json(json);
        console.log(` Reserva efectuada del hotel con id ${json.idHotel} para el cliente ${json.nombrereserva}`);
        
        fetch(queURL, {
            method: 'POST',
            body: JSON.stringify(nuevaReserva),
            headers: {
                        'Content-Type': 'application/json' 
                     }
        })
        .then(resp => resp.json())
        .then(json => {
            res.json(json); //devuelve directamente el json
        });

    });

});

app.delete('/api/hoteles/:idHotel', (req, res, next) => {

    const idHotel = req.params.idHotel;
    const queURL = `${URL_MOD_DB}/hoteles/${idHotel}`;

    fetch(`${URL_PROVHOTELES}/${idHotel}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {

        fetch(queURL, {
            method: 'DELETE',
            headers: {
                        'Content-Type': 'application/json' 
                     }
        })
        .then(resp => resp.json())
        .then(json => {
            res.json(json); //devuelve directamente el json
        });

    });

});
