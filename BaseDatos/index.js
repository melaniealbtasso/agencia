'use strict'

const config = require('../config');
const servicioToken = require('../Servicios/servicioToken');

const port = process.env.PORT || 3004
// const URL_PROVCOCHES = `https://localhost:3011/api/provcoches`;
// const URL_PROVHOTELES = `https://localhost:3013/api/provhoteles`;
// const URL_PROVVUELOS = `https://localhost:3012/api/provvuelos`;
// const URL_USUARIOS = `https://localhost:3000/api/usuarios`;
// const URL_COCHES = `https://localhost:3001/api/coches`;
const URL_MOD_DB = `https://localhost:3004/api/database`;
const URL_DB = config.db;
//const salt = 10;

const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const fetch = require('node-fetch');
const { json } = require('express');
const https = require('https');
const fs = require('fs');
const helmet = require('helmet');
const bcrypt = require('bcrypt');

const HTTP_OPTIONS = {
    key: fs.readFileSync('../cert/key.pem'),
    cert: fs.readFileSync('../cert/cert.pem')
}
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const app = express();

var db = mongojs(URL_DB);
var id = mongojs.ObjectID;

app.use(helmet());
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

function auth(req, res, next) {
    if (!req.headers.authorization){
        res.status(401).json({
            result: "KO",
            mensaje: "No se ha enviado el token en la cabereca autorization"
        });
        return next(new Error("Falta el token"));
    }

    if( req.headers.authorization.split(" ")[1] === "melanie"){
        return next();
    }

    res.status(401).json({
        result:"KO",
        mensaje: "Acceso no autorizado a este servicio"
    });

    return next(new Error("Acceso no autorizado"));
};

https.createServer(HTTP_OPTIONS, app)
.listen(port, (request, response) => {
    console.log(`API GESTIÓN DE BASE DE DATOS ejecutándose en ${URL_MOD_DB}`);
});

/**
 * BASE DE DATOS DE USUARIO
 */

// app.get('/api/database/usuarios', (req, res, next) => {
    
//     db.usuarios.find((err, elementos) =>{
//         if(err) return next(err);
        
//         console.log(elementos);
//         res.json({
//             result: 'OK',
//             coleccion: 'usuarios',
//             elemetos: elementos
//         });
//     });
// });

const identificadores = {
    usuarios: "nombreusuario",
    coches: "nombreusuario",
    vuelos: "nombreusuario",
    hoteles: "nombreusuario",
    pagos: "nombreusuario",
    paquetes: "nombreusuario"
}

const identificadoresDelete = {
    usuarios: "nombreusuario",
    coches: "idCoche",
    vuelos: "idVuelo",
    hoteles: "idHotel",
    pagos: "idPago",
    paquetes: "idPaquete"
}

app.get('/api/database/usuarios', (req, res, next) => {
    
    db.usuarios.find((err, elementos) =>{
        if(err) return next(err);
        
        console.log(elementos);
        res.json({
            result: 'OK',
            coleccion: 'usuarios',
            elemetos: elementos
        });
    });
});

app.post('/api/database/login', (req, res, next) => {

    const nombreusuario = req.body.nombreusuario;
    const password = req.body.password;

    db.usuarios.findOne({nombreusuario: nombreusuario}, (err, usuario) => {
        if(err || !usuario) {

            console.log(`No se ha encontrado en la BD el usuario ${nombreusuario}`);

            res.status(404).json({
                result: 'KO',
                error: "Usuario no registrado"
            });

        } else {
            bcrypt.compare(password, usuario.password, (erro, OKpassword) => {

                if(err) return next(err);

                if (!OKpassword) {

                    console.log(`Contraseña incorrecta para el usuario ${nombreusuario}`);

                    res.status(404).json({
                        result: 'KO',
                        error: "Contraeña incorrecta"
                    });

                } else {
                    
                    console.log(`El usuario ${nombreusuario} ha iniciado sesión`);

                    const token = servicioToken.crearToken(usuario);
                    servicioToken.comprobarToken(token)
                    .then(resultado => {
                        res.status(200).json({
                            result: 'OK',
                            usuario: resultado,
                            token: token
                        });
                    });
                }
            });
        }  

    });

});

app.get('/api/database/:collecion/:id', (req, res, next) => {

   const collecion = req.params.collecion;
   const id = req.params.id;

   const parametro = identificadores[collecion];
   var q = {};
   q[parametro] = id;

   db[collecion].find(q, (err, resultado) => {

       if (err || !resultado) return next(err);

       console.log('Encontrado');
       res.status(200).json({
            result: 'OK',
            elemento: resultado
       });

   });

});

app.post('/api/database/:collecion', (req, res, next) => {

    const collecion = req.params.collecion;
    const nuevoElemento = req.body;

    const parametro = identificadoresDelete[collecion];
    var q = {};

    if(collecion === "usuario"){
        q[parametro] = req.body.nombreusuario;
    } else if (collecion === "coches") {
        q[parametro] = req.body.idCoche;
    } else if ( collecion === "vuelos" ) {
        q[parametro] = req.body.idVuelo;
    } else if ( collecion === "hoteles" ) {
        q[parametro] = req.body.idHotel;
    } else if ( collecion === "pagos" ) {
        q[parametro] = req.body.idPago;
    } else if ( collecion === "paquetes" ) {
        q[parametro] = req.body.idPaquete;
    }

 
    db[collecion].findOne(q, (err, resultado) => {

        if (err) return next(err);
        
        console.log(`${resultado}`);

        if (!resultado) {

            db[collecion].save(nuevoElemento, (err, elementoGuardado) => {
 
                if (err || !elementoGuardado) return next(err);
         
                console.log('Guardado');
                res.status(200).json({
                     result: 'OK',
                     elemento: elementoGuardado
                });
         
            });

        } else {
            console.log('ERROR');
                res.status(400).json({
                     result: 'KO',
                     mensaje: "No se ha podido crear el elemento porque ya existe uno en la BD con ese identificador"
                });
        }
 
    });
 
 });

 app.put('/api/database/:collecion/:id', (req, res, next) => {

    const collecion = req.params.collecion;
    const id = req.params.id;
    const nuevosDatos = req.body;
 
    const parametro = identificadores[collecion];
    var q = {};
    q[parametro] = id;
 
    db[collecion].update(
        q,
        {$set: nuevosDatos},
        {safe: true, multi: false},
        (err, resultado) => {
 
        if (err || !resultado) return next(err);
 
        console.log('Modificado');
        res.status(200).json({
             result: 'OK',
             collecion: collecion,
             identificador: id,
             elemento: resultado
        });
 
    });
 
 });

app.delete('/api/database/:collecion/:id', (req, res, next) => {

    const collecion = req.params.collecion;
    const id = req.params.id;
 
    const parametro = identificadoresDelete[collecion];
    var q = {};
    q[parametro] = id;

    db[collecion].remove(q, (err, resultado) => {
 
        if (err || !resultado) return next(err);
 
        console.log('Encontrado');
        res.status(200).json({

            result: 'OK',
            collecion: collecion,
            parametro: parametro,
            identificador: id,
            resultado: resultado

        });
 
    });
 
 });