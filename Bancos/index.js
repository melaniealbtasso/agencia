'use strict'

const port = process.env.PORT || 3015
const config = require('../config');
const URL_BANCOS = `https://localhost:${port}/api/bancos`;
const URL_DB = config.db;

const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const querystring = require('query-string');
const fetch = require('node-fetch');
const { json } = require('express');
const https = require('https');
const fs = require('fs');
const { runInNewContext } = require('vm');

const HTTP_OPTIONS = {
    key: fs.readFileSync('../cert/key.pem'),
    cert: fs.readFileSync('../cert/cert.pem')
}
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const app = express();

var db = mongojs(URL_DB);
var id = mongojs.ObjectID;

app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

https.createServer(HTTP_OPTIONS, app)
.listen(port, (request, response) => {
    console.log(`API GESTIÓN DE BANCOS ejecutándose en ${URL_BANCOS}`);
});

function oferID(min, max) {
    return Math.floor(Math.random() * (max + min) + min);
}

app.post('/api/bancos/', (req, res, next) => {

    const prueba = oferID (1000000, 9999999);
    const idPago = oferID(1000000, 9999999);

    if (prueba > 5000000) {

        res.status(200).json({
            resultado: 'OK',
            idPago: idPago.toString(),
            cantidad: req.body.cantidad,
            tarjeta: req.body.tarjeta
        });

    } else {
        res.status(400).json({
            resultado: 'KO',
            mensaje: "NO SE HA PODIDO REALIZAR EL PAGO"
        });
    }
    
});

app.delete('/api/bancos/:idPago', (req, res, next) => {

    res.status(200).json({
        
        resultado: 'OK'
 
    });
    
 });

