'use strict'

const jwt = require('jwt-simple');
const moment = require('moment');
const config = require('../config');

const SECRET = config.secret;
const EXP_TIME = config.TOKEN_EXP_TIME;

function crearToken(usuario){
    const payload = {
        sub: usuario.nombreusuario,
        iat: moment().unix(),
        exp: moment().add(EXP_TIME, 'minutes').unix()
    }
    return jwt.encode(payload, SECRET);
}

function comprobarToken(token) {
    return new Promise((resolve, reject) => {
        try {
            const payload = jwt.decode(token, SECRET);
            if (payload.exp <= moment().unix()) {
                reject({
                    status: 401,
                    message: 'TOKEN CADUCADO'
                });
            }
            resolve(payload.sub);
        } catch (err) {
            reject({
                status: 500,
                message: 'TOKEN VÁLIDO'
            });
        }
    });
}

module.exports = {
    crearToken,
    comprobarToken
}