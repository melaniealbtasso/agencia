'use strict'

const port = process.env.PORT || 3005;
const config = require('../config');
const URL_BANCOS = `https://localhost:3015/api/bancos`;
const URL_PAGOS = `https://localhost:${port}/api/pagos`;
const URL_MOD_DB = `https://localhost:3004/api/database`;
const URL_DB = config.db;

const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const querystring = require('query-string');
const fetch = require('node-fetch');
const { json } = require('express');
const https = require('https');
const fs = require('fs');

const HTTP_OPTIONS = {
    key: fs.readFileSync('../cert/key.pem'),
    cert: fs.readFileSync('../cert/cert.pem')
}
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const app = express();

// var db = mongojs(URL_DB);
// var id = mongojs.ObjectID;

const helmet = require('helmet');
app.use(helmet());
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

https.createServer(HTTP_OPTIONS, app)
.listen(port, (request, response) => {
    console.log(`API GESTIÓN DE PAGOS ejecutándose en ${URL_PAGOS}`);
});

app.get('/api/pagos/:nombreusuario', (req, res, next) => {

    const nombreusuario = req.params.nombreusuario;

    fetch(`${URL_MOD_DB}/pagos/${nombreusuario}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        res.status(200).json(json);
        console.log(` Las ofertas se han encontrado!!`);
    });
});


app.post('/api/pagos', (req, res, next) => {

    const nuevoPago=req.body;

    const queURL = `${URL_MOD_DB}/pagos`;

    fetch(`${URL_BANCOS}`, {
        method: 'POST',
        body: JSON.stringify(nuevoPago),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {

        if (json.resultado === 'OK'){
            delete json['resultado'];
            json.nombreusuario = nuevoPago.nombreusuario;
    
            fetch(queURL, {
                method: 'POST', 
                body: JSON.stringify(json),
                headers: {
                            'Content-Type': 'application/json' 
                         }
            })
            .then(resp => resp.json())
            .then(json => {
                res.json(json); //devuelve directamente el json
            });
        } else {
            res.status(400).json(json)
        }

    });

});

app.delete('/api/pagos/:idPago', (req, res, next) => {

    const idPago = req.params.idPago;
    const queURL = `${URL_MOD_DB}/pagos/${idPago}`;

    fetch(`${URL_BANCOS}/${idPago}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {

        fetch(queURL, {
            method: 'DELETE',
            headers: {
                        'Content-Type': 'application/json' 
                     }
        })
        .then(resp => resp.json())
        .then(json => {
            res.json(json); //devuelve directamente el json
        });

    });

});
