'use strict'

const port = process.env.PORT || 3100
const URL_GATEWAY = `https://localhost:${port}/api`;
const URL_USUARIOS = `https://localhost:3000/api/usuarios`;
const URL_COCHES = `https://localhost:3001/api/coches`;
const URL_VUELOS = `https://localhost:3002/api/vuelos`;
const URL_HOTELES = `https://localhost:3003/api/hoteles`;
const URL_LOGIN = `https://localhost:3000/api/login`;
const servicioToken = require('../Servicios/servicioToken');

const express = require('express');
const logger = require('morgan');
const mongojs=require('mongojs');
const https = require('https');
const fs = require('fs');
const moment = require('moment');
const fetch = require('node-fetch');
const { url } = require('inspector');

const app = express();

const HTTP_OPTIONS = {
    key: fs.readFileSync('../cert/key.pem'),
    cert: fs.readFileSync('../cert/cert.pem')
}

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

function auth(req, res, next) {
    if (!req.headers.authorization){
        res.status(401).json({
            result: "KO",
            mensaje: "No se ha enviado el token en la cabereca autorization"
        });
        return next(new Error("Falta el token"));
    }

    try {

        const token = req.headers.authorization.split(" ")[1];
        servicioToken.comprobarToken(token).then(nombreusuario => {

            if(req.params.nombreusuario != null && req.body.nombreusuario != null) {
                //console.log("1");
                if (req.params.nombreusuario == nombreusuario || req.body.nombreusuario == nombreusuario) {
                    //console.log("2");
                    return next();
                }
            } else if (req.params.nombreusuario == nombreusuario || req.body.nombreusuario == nombreusuario) {
                //console.log("3");
                return next();
            }
            
            res.status(401).json({
                result:"KO",
                mensaje: "Acceso no autorizado a este servicio"
           });

        });

    }  catch (err) {
        console.log(err);
    }

}

https.createServer(HTTP_OPTIONS, app)
.listen(port, (request, response) => {
    console.log(`API GATEWAY ejecutándose en ${URL_GATEWAY}`);
});

/*

                GESTIÓN USUARIO

*/

//Muestra todos los usuarios
// app.get('/api/usuarios', auth, (req, res, next) => {

//     const queToken = req.params.token;
//     const queURL = `${URL_USUARIOS}`;
//     console.log(queURL);

//     fetch(queURL, {
//         method: 'GET',
//         headers: {
//                     'Content-Type': 'application/json' ,
//                     'Authorization': `Beare ${queToken}`
//         }
//     })
//     .then(res => res.json())
//     .then(json => {
//         res.status(200).json(json); //devuelve directamente el json
//     });
// });

//Muestra usuario cuyo nombre de usuario sea {nombreusuario}
app.get('/api/usuarios/:nombreusuario', auth, (req, res, next) => {

    const queUsuario = req.params.nombreusuario;
    const queToken = req.params.token;
    const queURL = `${URL_USUARIOS}/${queUsuario}`;
    console.log(queURL);
    fetch(queURL, {
        method: 'GET',
        headers: {
                    'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        res.status(200).json(json); //devuelve directamente el json
    });
});

//Crear un nuevo usuario
app.post('/api/usuarios', (req, res, next) => {
    const nuevoUsuario=req.body;
    const queURL = `${URL_USUARIOS}`;
    const queToken = req.params.token;

    fetch(queURL, {
                        method: 'POST',
                        body: JSON.stringify(nuevoUsuario),
                        headers: {
                                    'Content-Type': 'application/json',
                                    'Authorization': `Beare ${queToken}` 
                                 }
                    })
        .then(resp => resp.json())
        .then(json => {
            res.status(200).json(json); //devuelve directamente el json
        })
});

app.put('/api/usuarios/:nombreusuario', auth, (req, res, next) => {

    const queUsuario = req.params.nombreusuario;
    const nuevoUsuario=req.body;
    const queURL = `${URL_USUARIOS}/${queUsuario}`;
    const queToken = req.params.token;

    fetch(queURL, {
                        method: 'PUT',
                        body: JSON.stringify(nuevoUsuario),
                        headers: {
                                    'Content-Type': 'application/json' 
                                 }
                    })
        .then(resp => resp.json())
        .then(json => {
            res.status(200).json(json); //devuelve directamente el json
        })
});

//Borrar usuario
app.delete('/api/usuarios/:nombreusuario', auth, (req, res, next) => {

    const queUsuario = req.params.nombreusuario;
    const nuevoUsuario=req.body;
    const queURL = `${URL_USUARIOS}/${queUsuario}`;
    const queToken = req.params.token;

    fetch(queURL, {
                        method: 'DELETE',
                        body: JSON.stringify(nuevoUsuario),
                        headers: {
                                    'Content-Type': 'application/json',
                                    'Authorization': `Beare ${queToken}` 
                                 }
                    })
        .then(resp => resp.json())
        .then(json => {
            res.status(200).json(json); //devuelve directamente el json
        })
});

app.post('/api/login', (req, res, next) => {
    const loginUsuario=req.body;
    const queURL = `${URL_LOGIN}`;

    fetch(queURL, {
                        method: 'POST',
                        body: JSON.stringify(loginUsuario),
                        headers: {
                                    'Content-Type': 'application/json'
                                 }
                    })
        .then(resp => resp.json())
        .then(json => {
            res.status(200).json(json); //devuelve directamente el json
        })
});

/*

            GESTIÓN COCHES

*/

app.get('/api/coches/', (req, res, next) => {

    const queries = req.url.split("?")[1];
    console.log(`${queries}`);
    const urlFetch = URL_COCHES + "?" + queries;

    fetch(`${urlFetch}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        res.status(200).json(json);
        console.log(` Las ofertas se han encontrado!!`);
    });
});

app.get('/api/coches/:nombreusuario', auth, (req, res, next) => {

    const nombreusuario = req.params.nombreusuario;

    fetch(`${URL_COCHES}/${nombreusuario}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        res.status(200).json(json);
        console.log(` Las ofertas se han encontrado!!`);
    });
});

app.post('/api/coches', auth, (req, res, next) => {

    const nuevoReserva=req.body;
    const queURL = `${URL_COCHES}`;

    fetch(queURL, {
                        method: 'POST',
                        body: JSON.stringify(nuevoReserva),
                        headers: {
                                    'Content-Type': 'application/json' 
                                 }
                    })
        .then(resp => resp.json())
        .then(json => {
            res.status(200).json(json); //devuelve directamente el json
        })
});

app.delete('/api/coches/:idCoche', (req, res, next) => {

    //const proveedor = req.params.proveedor;
    const idCoche = req.params.idCoche;
    const queURL = `${URL_COCHES}/${idCoche}`;

    fetch(queURL, {
                        method: 'DELETE',
                        headers: {
                                    'Content-Type': 'application/json' 
                                 }
                    })
        .then(resp => resp.json())
        .then(json => {
            res.status(200).json(json); //devuelve directamente el json
    });
});

/*

            GESTIÓN VUELOS

*/

app.get('/api/vuelos/', auth, (req, res, next) => {

    const queries = req.url.split("?")[1];
    console.log(`${queries}`);
    const urlFetch = URL_VUELOS + "/?" + queries;

    fetch(`${urlFetch}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        res.status(200).json(json);
        console.log(` Las ofertas se han encontrado!!`);
    });
});

app.get('/api/vuelos/:nombreusuario', auth, (req, res, next) => {

    const nombreusuario = req.params.nombreusuario;

    fetch(`${URL_VUELOS}/${nombreusuario}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        res.status(200).json(json);
        console.log(` Las ofertas se han encontrado!!`);
    });
});

app.post('/api/vuelos', auth, (req, res, next) => {

    const nuevoReserva=req.body;
    const queURL = `${URL_VUELOS}`;

    fetch(queURL, {
                        method: 'POST',
                        body: JSON.stringify(nuevoReserva),
                        headers: {
                                    'Content-Type': 'application/json' 
                                 }
                    })
        .then(resp => resp.json())
        .then(json => {
            res.status(200).json(json); //devuelve directamente el json
    });
});

app.delete('/api/vuelos/:proveedor/:idvuelo', auth, (req, res, next) => {

    const proveedor = req.params.proveedor;
    const idvuelo = req.params.idvuelo;
    const queURL = `${URL_VUELOS}/${proveedor}/${idvuelo}`;

    fetch(queURL, {
                        method: 'DELETE',
                        headers: {
                                    'Content-Type': 'application/json' 
                                 }
                    })
        .then(resp => resp.json())
        .then(json => {
            res.status(200).json(json); //devuelve directamente el json
    });
});

/*

            GESTIÓN HOTELES

*/

app.get('/api/hoteles/', auth, (req, res, next) => {

    const queries = req.url.split("?")[1];
    console.log(`${queries}`);
    const urlFetch = URL_HOTELES + "?" + queries;

    fetch(`${urlFetch}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        res.status(200).json(json);
        console.log(` Las ofertas se han encontrado!!`);
    });
});

app.get('/api/hoteles/:nombreusuario', auth, (req, res, next) => {

    const nombreusuario = req.params.nombreusuario;

    fetch(`${URL_HOTELES}/${nombreusuario}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(json => {
        res.status(200).json(json);
        console.log(` Las ofertas se han encontrado!!`);
    });
});

app.post('/api/hoteles', auth, (req, res, next) => {

    const nuevoReserva=req.body;
    const queURL = `${URL_HOTELES}`;

    fetch(queURL, {
                        method: 'POST',
                        body: JSON.stringify(nuevoReserva),
                        headers: {
                                    'Content-Type': 'application/json' 
                                 }
                    })
        .then(resp => resp.json())
        .then(json => {
            res.status(200).json(json); //devuelve directamente el json
    });
});

app.delete('/api/hoteles/:proveedor/:idHotel', auth, (req, res, next) => {

    const proveedor = req.params.proveedor;
    const idHotel = req.params.idHotel;
    const queURL = `${URL_HOTELES}/${proveedor}/${idHotel}`;

    fetch(queURL, {
                        method: 'DELETE',
                        headers: {
                                    'Content-Type': 'application/json' 
                                 }
                    })
        .then(resp => resp.json())
        .then(json => {
            res.status(200).json(json); //devuelve directamente el json
    });
});